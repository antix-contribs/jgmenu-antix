#!/bin/bash
file="/usr/share/applications/exo-preferred-applications.desktop"
#Get system language (to allow localization):
lang=$(locale | grep LANG | cut -d= -f2 | cut -d. -f1)
#hack to fix languages that are identified in .desktop files by only 2 characters, and not 4 (5 counting the _), by comparing text that's before the "_" to the text that after that, converted to lower case, if it matches, use only the leters before the "_"
l1=$(echo $lang |cut -d_ -f1)
l2=$(echo $lang |cut -d_ -f2)
l2_converted=$(echo "${l2,,}")
if [ $l1 = $l2_converted ]; then lang=$l1; fi
# Get app name from .desktop file
name1=$(grep -o -m 1 '^Name=.*' $file| cut -d= -f2)
###Get localized app name:
name2=$name1
translated_name1=$(grep -o -m 1 "^Name\[$lang\]=.*" $file)
[ -z "$translated_name1" ] && note=$"No localized name found, using the original one" || name2=$translated_name1
name1=$name2
#Generate "virgin" append file file (with the "favorites manager" and the "terminal" entries only--- note- this script is for testing poporses- in the final version we should use our "tim" for jgmenu- a GUI way to manage Favourites
echo ${name}:, desktop-defaults-run -te ~/.config/jgmenu/append.csv > ~/.config/jgmenu/append.csv  
echo "^sep()" >> ~/.config/jgmenu/append.csv >> ~/.config/jgmenu/append.csv
echo Terminal,roxterm,utilities-terminal >> ~/.config/jgmenu/append.csv
echo "^sep()" >> ~/.config/jgmenu/append.csv >> ~/.config/jgmenu/append.csv

